package com.safexpay.android.UI.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.safexpay.android.Model.PaymentMode;
import com.safexpay.android.Model.SavedCards;
import com.safexpay.android.R;
import com.safexpay.android.UI.Activity.BaseActivity;
import com.safexpay.android.UI.Activity.PaymentDetailActivity;
import com.safexpay.android.UI.Adapter.SavedCardsAdapter;
import com.safexpay.android.Utils.CardTypes;
import com.safexpay.android.Utils.CardUtils;
import com.safexpay.android.Utils.RecyclerUtils;
import com.safexpay.android.Utils.SessionStore;
import com.safexpay.android.ViewModel.BrandingDetailsViewModel;
import com.safexpay.android.databinding.FragmentCardBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardFragment extends BaseFragment implements View.OnClickListener, View.OnKeyListener {

    private FragmentCardBinding binding;
    private PaymentDetailActivity activity;
    private Mode mode;
    private int selectedTenure;
    private String selectedBankCode;
    private static final String FRAGMENT_NAME = "CardFragment";
    private int cardNumberSelection;
    private CustomTextWatcher textWatcher;
    private String payModeId,PgId,SchemeId;
    private List<SavedCards> savedCardsList = new ArrayList<>();
    private List<PaymentMode> paymentModeList = new ArrayList<>();
    private BrandingDetailsViewModel detailsViewModel;
    CardTypes CardType;
    String Card_Type;
    LogInFragment logInFragment;


    public CardFragment() {
        // Required empty public constructor
    }

    public static CardFragment getCardForm(Mode mode) {
        CardFragment form = new CardFragment();
        form.mode = mode;
        return form;
    }

    public static CardFragment getCardForm(Mode mode, List<SavedCards> savedCardsList, String payModeId, String PgId,String SchemeId,List<PaymentMode> paymentModeList) {
        CardFragment form = new CardFragment();
        form.mode = mode;
        form.savedCardsList = savedCardsList;
        form.payModeId = payModeId;
        form.PgId =PgId;
        form.SchemeId = SchemeId;
        form.paymentModeList = paymentModeList;
        return form;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCardBinding.inflate(inflater, container, false);
        activity = (PaymentDetailActivity) getActivity();
        init();
        return binding.getRoot();
    }

    private void init() {
        //setting recyclerview
        detailsViewModel = ViewModelProviders.of(this).get(BrandingDetailsViewModel.class);
        detailsViewModel.init();
        observerViewModel();

        binding.savedCardsRecycler.setAdapter(new SavedCardsAdapter(getContext(), savedCardsList, payModeId));
        binding.savedCardsRecycler.addItemDecoration(new RecyclerUtils.LinePagerIndicatorDecoration());
        new RecyclerUtils.ScrollByOneItem().attachToRecyclerView(binding.savedCardsRecycler);

        //click listeners
        binding.navBackCard.setOnClickListener(this);
        binding.savedCardSdkTv.setOnClickListener(this);
        binding.showHideArrowIvCards.setOnClickListener(this);

        //editboxes
        binding.cardNumberEtSdk.setNextFocusDownId(R.id.month_et_sdk);
        binding.cardNumberEtSdk.addTextChangedListener(textWatcher = new CustomTextWatcher());
        binding.cardNumberEtSdk.setOnKeyListener(this);
        binding.monthEtSdk.setNextFocusDownId(R.id.cvv_et_sdk);



        SessionStore.PG_ID = PgId;
        //SessionStore.SCHEME_ID = SchemeId;


        binding.cvvEtSdk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (binding.cvvEtSdk.getText().toString().trim().length()>0){
                    SessionStore.CARD_CVV = binding.cvvEtSdk.getText().toString().trim();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.monthEtSdk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.monthEtSdk.getText().toString().trim().length()>0){
                    SessionStore.CARD_EXP_DATE = binding.monthEtSdk.getText().toString().trim();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.nameOnCardEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (binding.nameOnCardEt.getText().toString().trim().length()>0){
                    SessionStore.CARD_NAME =  binding.nameOnCardEt.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.monthEtSdk.addTextChangedListener(new TextWatcher() {
            private int previousLength = 0, currentLength = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                previousLength = binding.monthEtSdk.getText().toString().trim().length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                currentLength = binding.monthEtSdk.getText().toString().trim().length();
            }

            @Override
            public void afterTextChanged(Editable s) {
                String date = s.toString().trim().replaceAll(" ", "");
                String modifiedDate = date;

                boolean backPressed = previousLength > currentLength;
                if (backPressed) {
                    if (date.length() == 3) {
                        modifiedDate = date.substring(0, 2);
                    }
                } else {
                    if (date.length() == 2) {
                        modifiedDate = date + "/";
                    } else if (previousLength == 2) {
                        modifiedDate = date.substring(0, 2) + "/" + date.substring(2, date.length());
                    }
                }

                applyDateText(binding.monthEtSdk, this, modifiedDate);
                if (modifiedDate.length() == 5) {
                    binding.cvvEtSdk.requestFocus();
                }
            }
        });

        binding.cardNumberEtSdk.post(new Runnable() {
            @Override
            public void run() {
                binding.cardNumberEtSdk.requestFocus();
                InputMethodManager lManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (lManager != null) {
                    lManager.showSoftInput(binding.cardNumberEtSdk, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });
    }
    private void slide_down() {
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
        if (a != null) {
            a.reset();
            binding.savedCardsRecycler.clearAnimation();
            binding.savedCardsRecycler.startAnimation(a);
        }
    }
    public void toggle_contents(View v) {
        if (v.getVisibility() == View.VISIBLE) {
            v.animate().alpha(0.0f).setDuration(0);
            v.setVisibility(View.GONE);
            binding.showHideArrowIvCards.setRotation(90);
        } else {
            v.setVisibility(View.VISIBLE);
            v.animate().alpha(1.0f).setDuration(0);
            binding.showHideArrowIvCards.setRotation(-90);
            slide_down();
        }
    }

    private void applyDateText(TextInputEditText dateBox, TextWatcher textWatcher, String modifiedDate) {
        dateBox.removeTextChangedListener(textWatcher);
        dateBox.setText(modifiedDate);
        dateBox.setSelection(modifiedDate.length());
        dateBox.addTextChangedListener(textWatcher);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.saved_card_sdk_tv || id == R.id.show_hide_arrow_iv_cards) {
            toggle_contents(binding.savedCardsRecycler);
        } else if (id == R.id.nav_back_card) {
            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
                SessionStore.clearPaymentIds();
                SessionStore.PG_ID = "";
                SessionStore.SCHEME_ID = "";
                SessionStore.PAYMODE_ID = "";
                SessionStore.CARD_TYPE = "";
                SessionStore.CARD_NAME = "";
                SessionStore.PAYMODE_ID = "";
            }
        }
    }

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideKeyboard();
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                binding.cardNumberEtSdk.removeTextChangedListener(textWatcher);
                return true;
            }
        } else if (event.getAction() == KeyEvent.ACTION_UP) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                binding.cardNumberEtSdk.addTextChangedListener(textWatcher);
                return true;
            }
        }
        return false;
    }

    enum Mode {
        DebitCard,
        CreditCard,
        EMI
    }

    private class CustomTextWatcher implements TextWatcher {

        private int drawable = 0;
        private int previousLength = 0, currentLength = 0;
        private CardTypes cardType = CardTypes.UNKNOWN;
        private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
        private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
        private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
        private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
        private static final char DIVIDER = '-';

        public CustomTextWatcher() {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            previousLength = binding.cardNumberEtSdk.getText().toString().trim().length();

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String cardNumber = binding.cardNumberEtSdk.getText().toString();
            cardNumber = cardNumber.replaceAll(" ", "");

            // TODO this will be triggered for every text change which may not be required
            // Do this only for few characters not for entire card number
            cardType = CardUtils.getCardType(cardNumber);
            drawable = cardType.getImageResource();

            if (binding.cardNumberEtSdk.getText().toString().trim().length()>0){
                SessionStore.CARD_NUMBER = binding.cardNumberEtSdk.getText().toString().trim();
            }

            /*if (cardType == CardTypes.UNKNOWN || cardType == CardTypes.MAESTRO) {
                clearOptionalValidators();
            } else {
                addOptionalValidators();
            }*/

            if (cardNumber.isEmpty()) {
                drawable = R.drawable.ic_accepted_cards;
            }

            binding.cardNumberEtSdk.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);

            if (cardNumber.length() == cardType.getNumberLength()) {
                binding.monthEtSdk.requestFocus();
            }
            currentLength = binding.cardNumberEtSdk.getText().toString().trim().length();

            if (binding.cardNumberEtSdk.getText().toString().trim().length()==6){
                getCardType(binding.cardNumberEtSdk.getText().toString().trim());


            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (currentLength < previousLength) {
                return;
            }


            String cardNumber = s.toString().trim();
            cardNumberSelection = binding.cardNumberEtSdk.getSelectionStart();
            if (cardNumber.length() < 4) {
                return;
            }

            String modifiedCard;
            if (currentLength > previousLength) {
                String[] data = cardNumber.replaceAll(" ", "").split("");
                modifiedCard = "";
                 cardType = CardUtils.getCardType(cardNumber);
                  Card_Type = cardType+"";

                switch (cardType) {
                    case VISA:
                        break;
                    case MASTER_CARD:
                        break;
                    case DISCOVER:

                        break;
                    case RUPAY:
                        for (int index = 1; index < data.length; index++) {
                            modifiedCard = modifiedCard + data[index];
                            if (index == 4 || index == 8 || index == 12) {
                                modifiedCard = modifiedCard + " ";
                                cardNumberSelection++;
                            }
                        }

                        binding.cvvEtSdk.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cardType.getCvvLength())});
                        break;
                    case AMEX:
                        for (int index = 1; index < data.length; index++) {
                            modifiedCard = modifiedCard + data[index];
                            if (index == 4 || index == 11) {
                                modifiedCard = modifiedCard + " ";
                                cardNumberSelection++;
                            }
                        }
                        binding.cvvEtSdk.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cardType.getCvvLength())});
                        break;
                    case DINERS_CLUB:
                        for (int index = 1; index < data.length; index++) {
                            modifiedCard = modifiedCard + data[index];
                            if (index == 4 || index == 10) {
                                modifiedCard = modifiedCard + " ";
                                cardNumberSelection++;
                            }
                        }
                        binding.cvvEtSdk.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cardType.getCvvLength())});
                        break;
                    default:
                        modifiedCard = cardNumber;
                }
            } else {
                modifiedCard = cardNumber;
            }

            applyText(binding.cardNumberEtSdk, this, cardNumber);






        }
    }

    private void applyText(TextInputEditText editText, TextWatcher watcher, String text) {
        editText.removeTextChangedListener(watcher);
        editText.setText(text);
        cardNumberSelection = Math.min(cardNumberSelection, text.length());
        editText.setSelection(cardNumberSelection);
        editText.addTextChangedListener(watcher);

    }

    @Override
    public String getFragmentName() {
        return FRAGMENT_NAME;
    }

    private void observerViewModel(){


        detailsViewModel.getCardTypeLiveData().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String cardType) {
                SessionStore.CARD_TYPE = cardType;


                if (paymentModeList != null){

                    for (PaymentMode paymentMode : paymentModeList){

                        if (paymentMode.getPayModeId().equalsIgnoreCase(cardType) ) {

                            for (int i = 0; i < paymentMode.getPaymentModeDetailsList().size(); i++) {
                               if (cardType.equalsIgnoreCase("MASTER_CARD")) {
                                    Card_Type = "MASTERCARD";
                               }

                               if (paymentMode.getPaymentModeDetailsList().get(i).getSchemeDetailsResponse().getSchemeName().equalsIgnoreCase(Card_Type)) {

                                   String shema = paymentMode.getPaymentModeDetailsList().get(i).getSchemeDetailsResponse().getSchemeId();
                                   SessionStore.SCHEME_ID = shema;

                               }else {

                                   String shema = paymentMode.getPaymentModeDetailsList().get(i).getSchemeDetailsResponse().getSchemeId();
                                   SessionStore.SCHEME_ID = shema;


                               }

                           }
                    }
                    }
                }
            }
        });
    }
    public void getCardType(String cardNumber){

        if (cardNumber.trim().length() == 6){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("cardBin",cardNumber);
            jsonObject.addProperty("meid",SessionStore.merchantId);
            detailsViewModel.getCardType(jsonObject);
        }
    }
}
