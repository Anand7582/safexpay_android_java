package com.safexpay.android.UI.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.safexpay.android.Model.PaymentMode;
import com.safexpay.android.R;
import com.safexpay.android.UI.Fragment.NetBankingFragment;
import com.safexpay.android.Utils.SessionStore;
import com.safexpay.android.databinding.CustomBankItemBinding;

import java.util.List;

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.BankListViewHolder> {

    private Context context;
    private List<PaymentMode.PaymentModeDetailsList> paymentBankList;
    private CustomBankItemBinding binding;
    private String payModeId;
    int row_index = -1;


    public BankListAdapter(Context context, List<PaymentMode.PaymentModeDetailsList> paymentBankList, String payModeId) {
        this.context = context;
        this.payModeId = payModeId;
        this.paymentBankList = paymentBankList;

    }

    class BankListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView selectedIv;
        RelativeLayout bank_item_container;

        public BankListViewHolder(@NonNull View itemView) {
            super(itemView);
            binding.bankItemContainer.setOnClickListener(this);
            selectedIv = itemView.findViewById(R.id.selectedIv);
            bank_item_container = itemView.findViewById(R.id.bank_item_container);
            bank_item_container.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.bank_item_container) {
                int pos = getAdapterPosition();
                row_index = pos;
                paymentBankList.get(pos).setSelected(true);
                try {
                    SessionStore.PG_ID = paymentBankList.get(pos).getPgDetailsResponse().getPgId();
                    SessionStore.SCHEME_ID = paymentBankList.get(pos).getSchemeDetailsResponse().getSchemeId();
                    SessionStore.PAYMODE_ID = payModeId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();
            }

        }
    }
    public void selectCard(int position) {
        for (int i = 0; i < paymentBankList.size(); i++) {
            if (i == position){
                paymentBankList.get(position).setSelected(true);
            } else{
                paymentBankList.get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BankListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = CustomBankItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new BankListViewHolder(binding.getRoot());
    }
    @Override
    public void onBindViewHolder(@NonNull BankListViewHolder holder, int position) {
        PaymentMode.PaymentModeDetailsList item = paymentBankList.get(position);

        if ((item.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("State Bank Of India")) {
            binding.itemName.setText(String.format("%s", "SBI"));
            Glide.with(context).load(R.drawable.sbi_icon).centerCrop().into(binding.bankImageSdk);
        }
        if ((item.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("ICICI Bank")) {
            binding.itemName.setText(String.format("%s", "ICICI"));
            Glide.with(context).load(R.drawable.icici_icon).fitCenter().into(binding.bankImageSdk);
        }
        if ((item.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("YES Bank")) {
            binding.itemName.setText(String.format("%s", "Yes"));
            Glide.with(context).load(R.drawable.yes_icon).fitCenter().into(binding.bankImageSdk);
        }
        if ((item.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("Axis Bank")) {
            binding.itemName.setText(String.format("%s", "Axis"));
            Glide.with(context).load(R.drawable.axis_icon).fitCenter().into(binding.bankImageSdk);
        }
        if ((item.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("HDFC Bank")) {
            binding.itemName.setText(String.format("%s", "HDFC"));
            //Glide.with(context).load(item.getPgDetailsResponse().getPgIcon()).into(binding.bankImageSdk);
            Glide.with(context).load(R.drawable.hdfc_icon).fitCenter().into(binding.bankImageSdk);
        }
        if ((item.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("Kotak Bank")) {
            binding.itemName.setText(String.format("%s", "Kotak"));
            //binding.bankImageSdk.setImageDrawable(R.drawable.axis_icon);
            Glide.with(context).load(R.drawable.kotak_icon).centerCrop().into(binding.bankImageSdk);
        }



        // binding.itemName.setText(item.getPgDetailsResponse().getPgName());
        //Glide.with(context).load(item.getPgDetailsResponse().getPgIcon()).into(binding.bankImageSdk);
        holder.bank_item_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionStore.PG_ID = paymentBankList.get(position).getPgDetailsResponse().getPgId();
                SessionStore.SCHEME_ID = paymentBankList.get(position).getSchemeDetailsResponse().getSchemeId();
                SessionStore.PAYMODE_ID = payModeId;
                row_index = position;
                notifyDataSetChanged();
            }
        });
        if (row_index==position){
            if (NetBankingFragment.dialog.isShowing())
                NetBankingFragment.dialog.dismiss();
            NetBankingFragment.setSelectedBankText(item.getPgDetailsResponse().getPgName());
            holder.selectedIv.setVisibility(View.VISIBLE);
        }else {
            holder.selectedIv.setVisibility(View.GONE);
        }
    }
    @Override
    public int getItemCount() {
        return paymentBankList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
