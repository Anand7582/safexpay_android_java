package com.safexpay.android.UI.Fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.safexpay.android.Model.Bank;
import com.safexpay.android.Model.PaymentMode;
import com.safexpay.android.R;
import com.safexpay.android.UI.Adapter.BankListAdapter;
import com.safexpay.android.UI.Adapter.DialogBankListAdapter;
import com.safexpay.android.Utils.SessionStore;
import com.safexpay.android.databinding.DialogBankBinding;
import com.safexpay.android.databinding.FragmentNetBankingBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NetBankingFragment extends BaseFragment implements View.OnClickListener ,DialogBankListAdapter.RefreshBank {

    private FragmentNetBankingBinding binding;
    private static final String FRAGMENT_NAME = "NetBankingFragment";
    private List<PaymentMode.PaymentModeDetailsList> paymentBankList;
    private List<PaymentMode.PaymentModeDetailsList> paymentBankListTemp = new ArrayList<>();
    private DialogBankBinding bindingDialog;
    private String payModeId;
    private static TextView selectedBank;
    private SearchView searchView;
    private RecyclerView recyclerViewDialog;
    private RelativeLayout relativeLayout;
    public static Dialog dialog;


    public NetBankingFragment(List<PaymentMode.PaymentModeDetailsList> paymentBankList, String payModeId) {
        this.paymentBankList = paymentBankList;
        this.payModeId = payModeId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentNetBankingBinding.inflate(inflater, container, false);
        selectedBank = binding.netbankingSelectText;
        selectedBank.setOnClickListener(this);
        dialog = new Dialog(getContext(), R.style.DialogTheme);
        binding.netbankingListView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        init();
        return binding.getRoot();
    }

    private void init() {
        paymentBankListTemp.clear();

        for (PaymentMode.PaymentModeDetailsList paymentBank : paymentBankList) {
            paymentBank.setSelected(false);

            if ((paymentBank.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("State Bank Of India"))
                paymentBankListTemp.add(0, paymentBank);

            if ((paymentBank.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("ICICI Bank"))
                paymentBankListTemp.add(0, paymentBank);

            if ((paymentBank.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("YES Bank"))
                paymentBankListTemp.add(0, paymentBank);

            if ((paymentBank.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("Axis Bank"))
                paymentBankListTemp.add(0, paymentBank);

            if ((paymentBank.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("HDFC Bank"))
                paymentBankListTemp.add(0, paymentBank);

            if ((paymentBank.getPgDetailsResponse().getPgName().toLowerCase()).equalsIgnoreCase("Kotak Bank"))
                paymentBankListTemp.add(0, paymentBank);
        }

        binding.netbankingListView.setAdapter(new BankListAdapter(getActivity(), paymentBankListTemp, payModeId));
        binding.navBackNetbanking.setOnClickListener(this);
    }

    public void showDialog() {
        bindingDialog = DialogBankBinding.inflate(getLayoutInflater());
        dialog.setContentView(bindingDialog.getRoot());
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        searchView = bindingDialog.netbankingDialogSearchView;
        searchView.setIconifiedByDefault(false);
        searchView.onActionViewExpanded();
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
        recyclerViewDialog = bindingDialog.netbankingDialogRecycleView;
        recyclerViewDialog.setHasFixedSize(true);
        for (PaymentMode.PaymentModeDetailsList paymentBank : paymentBankList)
            paymentBank.setSelected(false);

        //recyclerViewDialog.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        //recyclerViewDialog.setAdapter(new BankListAdapter(getActivity(), paymentBankList, payModeId));
        recyclerViewDialog.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewDialog.setAdapter(new DialogBankListAdapter(getActivity(), paymentBankList, payModeId,this));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("TAG_TEXT", "onQueryTextChange: " + newText);
                List<PaymentMode.PaymentModeDetailsList> bankListTemp = new ArrayList<>();
                for (PaymentMode.PaymentModeDetailsList paymentBank : paymentBankList) {
                    if ((paymentBank.getPgDetailsResponse().getPgName().toLowerCase()).contains(newText.toLowerCase())) {
                        bankListTemp.add(paymentBank);
                        paymentBank.setSelected(false);
                    }
                }
                recyclerViewDialog.setAdapter(new DialogBankListAdapter(getActivity(), bankListTemp, payModeId,NetBankingFragment.this::refresh));
                return false;
            }
        });

        dialog.show();
    }

    public static void setSelectedBankText(String text) {
        selectedBank.setText(text);
    }



    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
    }

    @Override
    public String getFragmentName() {
        return FRAGMENT_NAME;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.nav_back_netbanking){
            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
                SessionStore.PG_ID = "";
                SessionStore.SCHEME_ID = "";
                SessionStore.PAYMODE_ID = "";
            }
        }

        if (id == R.id.netbanking_select_text) {
            showDialog();
        }
    }

    @Override
    public void refresh() {
        init();
    }
}
