package com.safexpay.android.UI.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.safexpay.android.Model.PaymentMode;
import com.safexpay.android.R;
import com.safexpay.android.UI.Fragment.NetBankingFragment;
import com.safexpay.android.Utils.SessionStore;
import com.safexpay.android.databinding.DialogBankRecycleItemBinding;

import java.util.List;

public class DialogBankListAdapter extends RecyclerView.Adapter<DialogBankListAdapter.DialogBankListViewHolder> {

    private Context context;
    private List<PaymentMode.PaymentModeDetailsList> paymentBankList;
    private DialogBankRecycleItemBinding binding;
    private String payModeId;
    int row_index = -1;
    public RefreshBank refreshBank;


    public DialogBankListAdapter(Context context, List<PaymentMode.PaymentModeDetailsList> paymentBankList, String payModeId , RefreshBank refreshBank) {
        this.context = context;
        this.payModeId = payModeId;
        this.paymentBankList = paymentBankList;
        this.refreshBank = refreshBank;

    }

    class DialogBankListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //ImageView selectedIv;
        //RelativeLayout bank_item_container;
        CardView bank_item_container;

        public DialogBankListViewHolder(@NonNull View itemView) {
            super(itemView);
            binding.dialogBankRecycleItemContainer.setOnClickListener(this);
            //bank_item_container = binding.dialogBankRecycleItemContainer;
            //bank_item_container.setOnClickListener(this);

            bank_item_container = itemView.findViewById(R.id.dialog_bank_recycle_item_container);
            bank_item_container.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.dialog_bank_recycle_item_container) {
                int pos = getAdapterPosition();
                row_index = pos;
                paymentBankList.get(pos).setSelected(true);

                try {
                    SessionStore.PG_ID = paymentBankList.get(pos).getPgDetailsResponse().getPgId();
                    SessionStore.SCHEME_ID = paymentBankList.get(pos).getSchemeDetailsResponse().getSchemeId();
                    SessionStore.PAYMODE_ID = payModeId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();
            }

        }
    }

    public void selectCard(int position) {
        for (int i = 0; i < paymentBankList.size(); i++) {
            if (i == position) {
                paymentBankList.get(position).setSelected(true);
            } else {
                paymentBankList.get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DialogBankListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DialogBankRecycleItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new DialogBankListViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull DialogBankListViewHolder holder, int position) {
        PaymentMode.PaymentModeDetailsList item = paymentBankList.get(position);
        binding.dialogBankRecycleItemBankText.setText(item.getPgDetailsResponse().getPgName());
        //Glide.with(context).load(item.getPgDetailsResponse().getPgIcon()).into(binding.bankImageSdk);
        holder.bank_item_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionStore.PG_ID = paymentBankList.get(position).getPgDetailsResponse().getPgId();
                SessionStore.SCHEME_ID = paymentBankList.get(position).getSchemeDetailsResponse().getSchemeId();
                SessionStore.PAYMODE_ID = payModeId;
                row_index = position;
                notifyDataSetChanged();
            }
        });
        if (row_index == position) {
            if (NetBankingFragment.dialog.isShowing())
                NetBankingFragment.dialog.dismiss();
            refreshBank.refresh();
            NetBankingFragment.setSelectedBankText(item.getPgDetailsResponse().getPgName());
            //NetBankingFragment.refreshBank(paymentBankList, payModeId);
        }
    }

    @Override
    public int getItemCount() {
        return paymentBankList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void filterSearch(List<PaymentMode.PaymentModeDetailsList> paymentBankList) {
        this.paymentBankList = paymentBankList;
        notifyDataSetChanged();
    }

    public interface RefreshBank {
        void refresh();
    }
}
